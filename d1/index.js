console.log('Hello World');


let posts = [];
let count = 1;


// adding post data

document.querySelector('#form-add-post').addEventListener('submit', (e) =>{
	// prevents the page from reloading after a button is clicked/triggered
	e.preventDefault();

	// adds the information from the frontend to our mock database
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value,
	});

	count++;

	console.log(posts);

	showPosts(posts);
	alert('Successfully Added.');	

});

//showPosts();

const showPosts = (posts) => {
	let postEntries = '';

	// we used forEach method so that each of the JSON inside our mock database will be rendered inside the #div-post-entries
	posts.forEach((post)=>{

	// in JavaScript, it is also possible to render HTML elements with the use of functions/methods + appropriate syntax such as template literals ``(backticks + ${})
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});
	// the div duplicates the objects inside the postEntries variable
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};


/*
	MINIACTIVITY
		create an editPost function that lets us transfer the id, title and body of each post entry to the second form (Edit Post)
			when the edit button has been clicked:
				the id of the div will be copied to the hidden id input filed of the Edit Post form
				the title of the div will be copied to the title input filed of the Edit Post form
				the body of the div will be copied to the body input filed of the Edit Post form
*/

// edit post data
const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};


// update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) =>{
	e.preventDefault();

	for (let i = 0; i < posts.length; i++){
		// .toString is necessary to convert the Number into String first before comparing the two values
		if(posts[i].id.toString() === document.querySelector(`#txt-edit-id`).value) {
			posts[i].title = document.querySelector(`#txt-edit-title`).value;
			posts[i].body = document.querySelector(`#txt-edit-body`).value;
		
			showPosts(posts);
			alert(`Successfully updated`);

			break;
		}

	};
});


//ACTIVITY delete post

const deletePost = (id) =>{
	// console.log(id);
	
	for (let i = 0; i < posts.length; i++){
		if(posts[i].id === id ){
			posts.splice(posts.indexOf(posts[i]),1);
			showPosts(posts);
			break;
		}
	};
};

